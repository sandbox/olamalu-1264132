 This module allows user to automaticaly upload  XML data to Google Merchant center.
 It uses the latest authentication and Google API version and is a successor to Google Base module.
 
 In this early development stage it relies on Ubercart's Product content type. 
 
 To install this module
 1. Follow the standard Drupal installation process (put into sites/all/modules) and enable - you may want Google Shopping Exta for CCK fields
 2. Register your domain for Google Apps http://code.google.com/apis/accounts/docs/RegistrationForWebAppsAuto.html
 2a. You can go to https://code.google.com/apis/console and when logged in go to 'Search API For Shopping' and switch On
 2b. Go to the Search API For Shopping page and to the link API Access
 2c. Enter the relevant information and choose 'Create Client ID'.  The Application is a 'web application' and the hostname your domain
 3. Copy the Client ID and the Secret into the relevant boxes in Site Configuration > Google Shopping
 4. Go to Store Configuration > Google Merchant Settings and put in your Merchant Center User ID (in your Google Account inside Merchant Center you should find an 'Account ID')
 
 This module provides two hooks allowing other developers to add additional elements or namespaces:
 hook_google_shopping_namespaces
 hook_google_shopping_elements
 example implementation is shown bellow 
 
 
 
  
 
 /*
 * example implementation of hook_google_shopping_elements and hook_google_shopping_namespaces
 *
 function google_shopping_google_shopping_namespaces(){
 $nss=array('myns'=>'http://www.prochazka.co.uk/namespace/my');
 return $nss;
 }

 function google_shopping_google_shopping_elements($node){
 $elements=array();
 $elements[]=array(
 'ns'=>'http://schemas.google.com/structuredcontent/2009/products',
 'tag'=>'brand',
 'attributes'=>array(
          'type'=>'text'),
 'value'=>'Pepsi'
 );
 return $elements;
 }
 */