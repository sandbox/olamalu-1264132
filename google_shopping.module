<?php

/*
 * author: roman@olamalu.com
 */

define('GOOGLE_SHOPPING_SCOPE', 'https://www.googleapis.com/auth/structuredcontent');
define('GOOGLE_SHOPPING_REQUEST_TOKEN_URI', 'https://accounts.google.com/o/oauth2/auth');
define('GOOGLE_SHOPPING_ACCESS_TOKEN_URI', 'https://accounts.google.com/o/oauth2/token');
define('GOOGLE_SHOPPING_REQUEST_TOKEN_REDIRECT_URI', '/oauth2callback');


/**
 * Implementation of hook_menu().
 */
function google_shopping_menu() {

  $items['admin/settings/google_shopping'] = array(
    'title' => 'Google shopping admin settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'google_shopping_access_settings_form'
    ),
    'access arguments' => array(
      'administer google shopping'
    ),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'google_shopping.admin.inc'
  );

  $items['admin/store/google_shopping'] = array(
    'title' => 'Google merchant settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'google_shopping_merchant_settings_form'
    ),
    'access arguments' => array(
      'use google shopping'
    ),
    'type' => MENU_NORMAL_ITEM
  );

  $items['oauth2callback'] = array(
    'title' => 'Oauth callback',
    'page callback' => 'get_oauth_access_token',
    'access arguments' => array(
      'use google shopping'
    ),
    'type' => MENU_CALLBACK,
  );

  $items['google_auth'] = array(
    'title' => 'Oauth',
    'page callback' => 'get_oauth_request_token',
    'access arguments' => array(
      'use google shopping'
    ),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implementation of hook_cron().
 */
function google_shopping_cron() {
  //First queue up any nodes which are products and not in our tracking table
  google_shopping_queue_nodes();
  //Then send in batches the nodes which need to go up to Google
  google_shopping_batch_send_nodes();
}

/**
 * Implementation of hook_node_api().
 */
function google_shopping_nodeapi(& $node, $op, $a3 = NULL, $a4 = NULL) {
  //this is product as defined by ubercart
  if (in_array($node->type, variable_get('google_shopping_node_types', array()))) {
    switch ($op) {
      //updating products
      case 'update':
      case 'insert':
        _google_shopping_update_item($node);
        break;
      case 'delete':
        //Delete from our tracking table
        db_query("DELETE FROM {google_shopping} g WHERE g.nid=%d",$node->nid);
        $item_id=$node->model;
        $merchant_id=variable_get('google_shopping_merchant_id', '');
        $access_token=$_SESSION['google_shopping_access_token'];
        $url='https://content.googleapis.com/content/v1/'. $merchant_id .'/items/products/generic/online:en:GB:'. $item_id .'?oauth_token='. $access_token;
        $data=('');//delete
        $headers=array('Content-Type' => 'application/atom+xml');
        $response=drupal_http_request($url, $headers, 'DELETE', $data, 1);
        if ($response->code == '200') {
          //success
          drupal_set_message(t('Product deleted from Google Merchant center successfully.'), 'status');
        }
        elseif ($response->code == '401') {
          //token has expired - get a new one
          $access_token=_google_shopping_refresh_access_token();
          if ($access_token != 'error') {
            $url='https://content.googleapis.com/content/v1/'. $merchant_id .'/items/products/generic/online:en:GB:'. $item_id .'?oauth_token='. $access_token;
            $response=drupal_http_request($url, $headers, 'DELETE', $data, 1);
            if ($response->code == '200') {
              drupal_set_message(t('Product deleted Google Merchant center successfuly.'), 'status');
            }
            else {
              drupal_set_message(t('An error occured while deleting product from Google Merchant Center.'), 'error');
            }
          }
          else {
            drupal_set_message(t('An error occured while deleting product from Google Merchant Center.'), 'error');
          }
        }
    }
  }
}

function _google_shopping_update_item($node, $batch=FALSE) {
  $merchant_id=variable_get('google_shopping_merchant_id', '');
  $refresh_token=variable_get('google_shopping_refresh_token', '');
  //check basic settings
  if (($merchant_id=='')||($refresh_token=='')) {
    if ($batch) {
      watchdog('gshopping', 'Merchant account not configured', WATCHDOG_WARNING);
    } else {
      drupal_set_message(t('Configure your Google Merchant account to use this functionality', 'warning'));
    }
  }
  else {
    //create xml product description
    $xmlstr = _google_shopping_create_xml($node);
    $access_token=$_SESSION['google_shopping_access_token'];
    $url="https://content.googleapis.com/content/v1/". $merchant_id ."/items/products/generic?oauth_token=". $access_token;
    $response=drupal_http_request($url, array('Content-Type' => 'application/atom+xml'), 'POST', $xmlstr, 1);
    if (($response->code=='200')||($response->code=='201')) {
      //success
      if ($batch) {
        watchdog('gshopping', 'Product %nid uploaded to Google Shopping', array('%nid'=>$node->nid), WATCHDOG_NOTICE);
      } else {
        drupal_set_message(t('Product uploaded to Google Merchant center successfully.'));
      }
    }
    elseif ($response->code=='401') {
      //access token has expired - get a new one
      $access_token=_google_shopping_refresh_access_token();
      if ($access_token != 'error') {
        $url="https://content.googleapis.com/content/v1/". $merchant_id ."/items/products/generic?oauth_token=". $access_token;
        $response=drupal_http_request($url, array('Content-Type' => 'application/atom+xml'), 'POST', $xmlstr, 1);
        if (($response->code=='200')||($response->code=='201')) {
          //success
          if ($batch) {
            watchdog('gshopping', 'Product '. $node->nid . ' uploaded to Google Shopping', WATCHDOG_NOTICE);
          } else {
            drupal_set_message(t('Product uploaded to Google Merchant center successfully.'));
          }
        }
        else {
          if ($batch) {
            watchdog('gshopping', 'Product %nid failed to upload to Google Shopping', array('%nid'=>$node->nid),  WATCHDOG_ERROR);
          } else {
            drupal_set_message(t('Error occured while attempting to connect to Google Merchant center. Please check your settings.', 'error'));
          }
        }
      }
      else {
        if ($batch) {
          watchdog('gshopping', 'Product %nid failed to upload to Google Shopping', array('%nid'=>$node->nid), WATCHDOG_ERROR);
        } else {
          drupal_set_message(t('Error occured while attempting to connect to Google Merchant center. Please check your settings.', 'error'));
        }
      }
    }
    else {
      //account not set up or invalid settings
      $error=$response->error;
      if ($batch) {
        watchdog('gshopping', 'Product %nid failed to upload to Google Shopping with error %error', array('%nid'=>$node->nid, '%error'=>$response->error), WATCHDOG_ERROR);
      } else {
        drupal_set_message(t('Check you Google Merchant settings to use this functionality.(Error: @error .)',array('@error'=>$error)), 'warning');
      }
    }
  }
  //Store the data in the resend table
  _google_shopping_update_resend($node, $reponse->code . ($response->error ? ' ' . $response->error : ''));
}

//initial  token request
function get_oauth_request_token() {
  $url=GOOGLE_SHOPPING_REQUEST_TOKEN_URI ."?client_id=". variable_get('google_shopping_client_id', '') .
 "&redirect_uri=". $GLOBALS['base_url'] . GOOGLE_SHOPPING_REQUEST_TOKEN_REDIRECT_URI ."&scope=". GOOGLE_SHOPPING_SCOPE ."&response_type=code";
  $response=drupal_http_request($url, array(), 'POST', NULL, 1);
  print($response->data); //this displays Google page 
}


function get_oauth_access_token() {
  $auth_code=$_GET['code'];
  if ($auth_code != '') {
    $url=GOOGLE_SHOPPING_ACCESS_TOKEN_URI;
    $d= 'code='. $auth_code .
 '&client_id='. variable_get('google_shopping_client_id', '') .
 '&client_secret='. variable_get('google_shopping_client_secret', '') .
 '&redirect_uri='. $GLOBALS['base_url'] . GOOGLE_SHOPPING_REQUEST_TOKEN_REDIRECT_URI .
 '&grant_type=authorization_code';
    $response=drupal_http_request($url, array('Content-Type' => 'application/x-www-form-urlencoded'), 'POST', $d, 1);
    //do we have error code or success?
    if ($response->code == '200') {
      $obj = json_decode($response->data);
      $access_token=$obj->access_token;
      $refresh_token=$obj->refresh_token;
      $_SESSION['google_shopping_access_token']=$access_token;
      variable_set('google_shopping_refresh_token', $refresh_token);
      drupal_set_message(t('Access token aquired successfully'), 'status');
    }
    else {
      //error
      drupal_set_message(t('Error occured when contacting Google API. Please contact your site administrator. (Error: @error.)',array('@error'=>$response->error)), 'error');
    }
  }
  else {
    //user refused authentication
    drupal_set_message(t('Authentication refused'), 'warning');
  }
  drupal_goto('admin/store/google_shopping');
}

/*
 * get new token when it expired
 */
function _google_shopping_refresh_access_token() {
  $url=GOOGLE_SHOPPING_ACCESS_TOKEN_URI;
  $d= 'client_id='. variable_get('google_shopping_client_id', '') .
 '&client_secret='. variable_get('google_shopping_client_secret', '') .
 '&refresh_token='. variable_get('google_shopping_refresh_token', '') .
 '&grant_type=refresh_token';
  $response=drupal_http_request($url, array('Content-Type' => 'application/x-www-form-urlencoded'), 'POST', $d, 1);

  if ($response->code == '200') {
    $obj = json_decode($response->data);
    $access_token=$obj->access_token;
    $_SESSION['google_shopping_access_token']=$access_token;
    return $access_token;
  }
  else return 'error';
}


function google_shopping_merchant_settings_form() {
  $form=array();
  $form['merchant_settings']=array();
  $form['merchant_settings']['merchant_id']=array(
    '#type' => 'textfield',
    '#title' => 'Merchant id',
    '#weight' => '1',
    '#description' => 'Merchant id is in 12345678 format',
    '#default_value' =>  variable_get('google_shopping_merchant_id', '')
  );

  $form['merchant_settings']['authenticate']=array(
   '#type' => 'submit',
   '#value' => 'Authenticate Google account',
   '#weight' => '10',
   '#submit' => array('google_oauth_request_submit')
  );
  $form['access_settings']['submit']=array(
   '#type' => 'submit',
   '#value' => 'Submit',
   '#weight' => '60',
   '#submit' => array('google_merchant_settings_submit')
  );
  return $form;
}

function google_oauth_request_submit($form, &$form_state) {
  drupal_goto('google_auth');
}

function google_merchant_settings_submit($form, &$form_state) {
  variable_set('google_shopping_merchant_id', check_plain($form_state['values']['merchant_id']));
}

/**
 * Creates XML description of this product node
 * this is based on ubercart keywords for time being
 * additional keyword mapping is provided via google_shopping_extra submodule
 * it is also possible to add more elements or namespaces by implementing module hooks
 * see README file for examples
 */
function _google_shopping_create_xml($node=NULL) {
  $product_link=isset($node->path) && $node->path != '' ? $GLOBALS['base_url'] .'/'. $node->path : $GLOBALS['base_url'] .'/node/'. $node->nid;
  $product_id= $node->model;
  $product_title=$node->title;
  $product_content=$node->body;
  $price=NULL;
  if (TRUE/* TODO:module exists? */) {
    //We need it with taxes - so have to set the context so that there are tax rates
    //See uc_vat_price_handler_alter - need to create and order object order->data['taxes']
    $order = new stdClass();
    $order->data = array('taxes' => uc_taxes_rate_load());
    $context = array(
      'revision' => 'altered',
      'type' => 'product',
      'class' => array('product', 'sell_price'),
      'subject' => array('node' => $node, 'order' => $order),
    );
    $precision = variable_get('uc_currency_prec', 2);
    $p = uc_price($node->sell_price, $context);
    $price = round($p, $precision);
  }


  $product_price=isset($price) ? $price : number_format($node->sell_price, 2, '.', '');
  isset($node->field_image_cache['0']['filepath']) ? $product_image=$node->field_image_cache['0']['filepath'] : NULL;
  $price_unit=variable_get('google_shopping_price_unit', 'gbp');
  $target_country=variable_get('google_shopping_target_country', 'GB');
  $default_condition=variable_get('google_shopping_condition', 'new');
  $namespaces=array('xmlns="http://www.w3.org/2005/Atom"',
    'xmlns:app="http://www.w3.org/2007/app"',
    'xmlns:gd="http://schemas.google.com/g/2005"',
    'xmlns:sc="http://schemas.google.com/structuredcontent/2009"',
    'xmlns:scp="http://schemas.google.com/structuredcontent/2009/products"');
  //add extra namespaces from google_shopping_namespaces hook implementation
  $extra_namespaces = module_invoke_all('google_shopping_namespaces');
  if (is_array($extra_namespaces)) {
    foreach ($extra_namespaces as $key => $value) {
      $namespaces[]='xmlns:'. $key .'="'. $value .'"';
    }
  }

  $xml = new SimpleXMLElement('<entry '. implode(' ', $namespaces) .' >
     <app:control>
        <sc:required_destination dest="ProductSearch"/>
    </app:control></entry>');
  //add elements from modules implementing hook first
  $extra_elements = module_invoke_all('google_shopping_elements', $node);
  if (is_array($extra_elements)) {
    foreach ($extra_elements as $extra_element) {
      $element=$xml->addChild($extra_element['tag'],
      isset($extra_element['value']) ? htmlentities($extra_element['value']) : NULL,
      isset($extra_element['ns']) && ($extra_element['ns'] != '') ? $extra_element['ns'] : NULL);
      if (isset($extra_element['attributes'])) {
        foreach ($extra_element['attributes'] as $key => $value) {
          $element->addAttribute($key, $value);
        }
      }
    }
  }
  //add missing required elements
  //note that some elements cannot be currently changed: price, link
  $str_xml=$xml->asXML();
  $xml=new SimpleXMLElement($str_xml);
  if (!isset($xml->title)) {
    $title = $xml->addChild('title', htmlentities($product_title));
    $title->addAttribute('type', 'text');
  }
  $link=$xml->addChild('link');
  $link -> addAttribute('rel', 'alternate');
  $link -> addAttribute('type', 'text/html');
  $link -> addAttribute('href', $product_link);
  if (!isset($xml->id))  {
    $id = $xml->addChild('id', $product_id, 'http://schemas.google.com/structuredcontent/2009');
  }
  if (!isset($xml->content)) {
    $content = $xml->addChild('content', htmlentities($product_content));
    $content -> addAttribute('type', 'text');
  }
  if (!isset($xml->country)) {
    $country=$xml->addChild('target_country', $target_country, 'http://schemas.google.com/structuredcontent/2009');
  }
  $price = $xml->addChild('price', $product_price, 'http://schemas.google.com/structuredcontent/2009/products');
  $price->addAttribute('unit', $price_unit);
  if (!isset($xml->condition)) {
    $condition = $xml -> addChild('condition', $default_condition, 'http://schemas.google.com/structuredcontent/2009/products');
  }
  isset($product_image) ? $image_link=$xml -> addChild('image_link', $GLOBALS['base_url'] .'/'. $product_image, 'http://schemas.google.com/structuredcontent/2009') : NULL;
  //set up product categories if the vocabulary is set up
  $vid=variable_get('google_shopping_taxonomy', '');
  if ($vid != '' && $vid != -1) {
    $term=taxonomy_node_get_terms_by_vocabulary($node, $vid);
    $product_categories= _google_shopping_create_categories_structure($term);
    $category=$xml->addChild('product_type', htmlentities($product_categories), 'http://schemas.google.com/structuredcontent/2009/products');
  }
  $xml->asXML();
  return $xml->asXML();
}

function _google_shopping_create_categories_structure($term) {
  $tid=0;
  //there should be only one item in an array - find its tid
  foreach ($term as $t) {
    $tid=$t->tid;
  }
  if ($tid == 0) {
    return 'error';
  }
  else {
    $categories=array();
    $parents=taxonomy_get_parents_all($tid);
    foreach ($parents as $parent) {
      $categories[]=$parent->name;
    }
    $str_categories='';
    for ($i=count($categories)-1;$i>=0;$i--) {
      $str_categories .=$categories[$i];
      if ($i!=0) {
        $str_categories .=' > ';
      }
    }
    return check_plain($str_categories);
  }
}

/**
 * Implementation of hook_perm().
 */
function google_shopping__perm() {
  return array('administer google shopping', 'use google shopping');
}
/**
 * Update the resend table with data about the node
 */
function _google_shopping_update_resend($node, $result='') {
  //If the data exists in the table update that, otherwise create fresh
  $t = db_fetch_object(db_query("SELECT g.* FROM {google_shopping} g WHERE g.nid=%d", $node->nid));
  if ($t) {
    $t->updated = time();
    if (variable_get('google_shopping_resend', 0)) {
      //Add number of days on in seconds
      $t->nextupdate = time() + (variable_get('google_shopping_resend', 0)*86400);
    }
    $t->result = $result;
    drupal_write_record('google_shopping', $t, 'nid');
  } else {
    $t = new stdClass();
    $t->nid = $node->nid;
    $t->updated = time();
    if (variable_get('google_shopping_resend', 0)) {
      //Add number of days on in seconds
      $t->nextupdate = time() + (variable_get('google_shopping_resend', 0)*86400);
    }
    $t->status = 0;
    $t->result = $result;
    drupal_write_record('google_shopping', $t);
  }
}

function google_shopping_queue_nodes() {
  //We should probably create a hook here and let others (ubercart/shopping) determine what to queue
  //For the moment we just queue all products
  $types = variable_get('google_shopping_node_types', array());
  if (count($types) > 0) {
    foreach($types as $type) {
      $q = db_query("SELECT n.nid FROM {node} n LEFT JOIN {google_shopping} g ON g.nid=n.nid WHERE g.nid IS NULL AND n.type='%s'", $type);
      while ($r = db_fetch_object($q)) {
      $t = new stdClass();
      $t->nid = $r->nid;
      $t->updated = 0;
      $t->nextupdate = time();
      $t->status = 0;
      drupal_write_record('google_shopping', $t);
      }
    }
  }
}

function google_shopping_batch_send_nodes() {
  //We need to make this configurable
  $q = db_query("SELECT g.* FROM {google_shopping} g WHERE g.nextupdate < %d and g.nextupdate != 0 LIMIT 20", time());
  while ($t = db_fetch_object($q)) {
    $node = node_load($t->nid);
    if ($node) {
      _google_shopping_update_item($node, TRUE);
    }
  }
}