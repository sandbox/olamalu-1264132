<?php
/**
 * @file
 * Administration functions for google_shopping module.
 */

function google_shopping_access_settings_form() {
  $form = array();

  $form['access_settings']['client_id']=array(
    '#type' => 'textfield',
    '#title' => 'Client id',
    '#weight' => '10',
    '#description' => 'Client id format similar to 123456789.apps.googleusercontent.com',
    '#default_value' =>  variable_get('google_shopping_client_id', ''),
  );

  $form['access_settings']['client_secret']=array(
     '#type' => 'textfield',
     '#title' => 'Client secret',
    '#weight' => '11',
    '#description' => 'Client secret ...',
     '#default_value' =>  variable_get('google_shopping_client_secret', '')
  );
  #-------------------------------------------------------------------------

  $form['shop_settings']['target_country']=array(
     '#type' => 'textfield',
     '#title' => 'Target country',
     '#weight' => '20',
     '#description' => 'Please enter the target country. Only single country  is currently supported.',
     '#default_value' =>  variable_get('google_shopping_target_country', 'GB')
  );

  $form['shop_settings']['currency']=array(
    '#type' => 'textfield',
    '#title' => 'Currency',
    '#weight' => '21',
    '#description' => 'Please enter the currency.',
    '#default_value' =>  variable_get('google_shopping_currency', 'gbp')
  );

  $form['shop_settings']['condition']=array(
    '#type' => 'textfield',
    '#title' => 'Default product condition',
    '#weight' => '23',
    '#description' => 'Please enter default condition.',
    '#default_value' => variable_get('google_shopping_condition', 'new')
  );

  $form['shop_settings']['taxonomy']=array(
    '#type' => 'select',
    '#title' => 'Select Google merchant vocabulary.',
    '#weight' => '25',
    '#description' => 'This is used for product categorization',
    '#options' =>  _google_shopping_get_product_vocabularies(),
    '#default_value' => _google_shopping_get_vocabulary_position(variable_get('google_shopping_taxonomy', 0))
  );
  $form['shop_settings']['resend']=array(
    '#type' => 'textfield',
    '#title' => 'Resend to Google Every (Days).',
    '#weight' => '26',
    '#description' => 'To resend products to Google every X Days. 0 for never.',
    '#default_value' => variable_get('google_shopping_resend', 0)
  );
  $form['shop_settings']['product_types']=array(
    '#type' => 'select',
    '#title' => 'Select Google merchant node types.',
    '#weight' => '27',
    '#description' => 'Select the nodes which get sent to google products',
    '#options' =>  node_get_types('names'),
    '#multiple' => TRUE,
    '#default_value' => variable_get('google_shopping_node_types', array())
  );
  $form['submit']=array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#weight' => '200'
  );
  return $form;
}

function _google_shopping_get_product_vocabularies() {
  $types = variable_get('google_shopping_node_types', array());
  //We make the presumption all product node types have the same vocabularies attached
  $vocs=taxonomy_get_vocabularies($types[0] ? $types[0] : '');
  $voc_names=array();
  $voc_names[]='none';
  foreach ($vocs as $voc) {
    $voc_names[]=$voc->name;
  }
  return $voc_names;
}

function _google_shopping_get_vocabulary_id($position) {
  if ($position == 0) {
    return -1; //none selected
  }
  else {
    $types = variable_get('google_shopping_node_types', array());
    //We make the presumption all product node types have the same vocabularies attached
    $vocs=taxonomy_get_vocabularies($types[0] ? $types[0] : '');
    $voc_names=array();
    $loop=0;
    $voc_id=-1;
    foreach ($vocs as $voc) {
      if ($loop == $position-1) {
        $voc_id=$voc->vid;
      }
      $loop++;
    }
    return $voc_id;
  }
}

/*
 * used  to retrieve default value
 */
function _google_shopping_get_vocabulary_position($vid) {
  if ($vid == -1) {
    return 0;
  }
  else {
    $loop=0;
    $position=0;
    $vocs=taxonomy_get_vocabularies('product');
    foreach ($vocs as $voc) {
      if ($voc->vid == $vid) {
        $position=$loop;
      }
      $loop++;
    }return $position+1;
  }
}

function google_shopping_access_settings_form_submit($form, &$form_state) {
  variable_set("google_shopping_client_id", check_plain($form_state['values']['client_id']));
  variable_set("google_shopping_client_secret", check_plain($form_state['values']['client_secret']));
  variable_set("google_shopping_target_country", check_plain($form_state['values']['target_country']));
  variable_set("google_shopping_currency", check_plain($form_state['values']['currency']));
  variable_set("google_shopping_condition", check_plain($form_state['values']['condition']));
  variable_set("google_shopping_resend", check_plain($form_state['values']['resend']));
  variable_set("google_shopping_taxonomy", _google_shopping_get_vocabulary_id($form_state['values']['taxonomy']));
  if (is_array($form_state['values']['product_types'])) {
    $s=array();
    foreach($form_state['values']['product_types'] as $t) {
      $s[] = check_plain($t);
    }
    variable_set("google_shopping_node_types",$s);
  } else if ($form_state['values']['product_types']){
    variable_set("google_shopping_node_types",array($form_state['values']['product_types']));
  }
  drupal_set_message(t('The configuration has been updated.'));
}